pub fn hamming_normalized_similarity(str1: &str, str2: &str) -> f64 {
    if str1.len() == 0 || str2.len() == 0 {
        return 0_f64;
    }
    let max_len = std::cmp::max(str1.chars().count(), str2.chars().count());
    let matching_chars = str1
        .chars()
        .zip(str2.chars())
        .filter(|(c1, c2)| c1 == c2)
        .count();
    return matching_chars as f64 / max_len as f64;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hamming_normalized_similarity_same_len_test() {
        // given
        let str1 = "test";
        let str2 = "tett";

        // when
        let normalized = hamming_normalized_similarity(&str1, &str2);

        // then
        assert_eq!(0.75_f64, normalized);
    }

    #[test]
    fn hamming_normalized_similarity_different_len_test() {
        // given
        let str1 = "tes";
        let str2 = "test";

        // when
        let normalized = hamming_normalized_similarity(&str1, &str2);

        // then
        assert_eq!(0.75_f64, normalized);
    }

    #[test]
    fn hamming_normalized_similarity_empty_str_test() {
        // given
        let str1 = "test";
        let str2 = "";

        // when
        let normalized = hamming_normalized_similarity(&str1, &str2);

        // then
        assert_eq!(0_f64, normalized);
    }
}
