use futures_util::{SinkExt, StreamExt, TryFutureExt};
use std::collections::HashMap;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};
use tokio::sync::{mpsc, RwLock};
use warp::ws::{Message, WebSocket};
use warp::Filter;

static ID_GENERATOR: AtomicUsize = AtomicUsize::new(1);

type Users = Arc<RwLock<HashMap<usize, mpsc::UnboundedSender<Message>>>>;

#[tokio::main]
async fn main() {
    let users = Users::default();
    let users = warp::any().map(move || users.clone());

    let websocket = warp::path("ws")
        .and(warp::ws())
        .and(users)
        .map(|ws: warp::ws::Ws, users| {
            ws.on_upgrade(move |socket| handle_connection(socket, users))
        });

    warp::serve(websocket).run(([127, 0, 0, 1], 8080)).await;
}

async fn handle_connection(ws: WebSocket, users: Users) {
    let id = ID_GENERATOR.fetch_add(1, Ordering::Relaxed);
    println!("User connected, assigned id {}", id);
    let (mut user_ws_tx, mut user_ws_rx) = ws.split();

    let (tx, mut rx) = mpsc::unbounded_channel();
    tokio::task::spawn(async move {
        while let Some(msg) = rx.recv().await {
            user_ws_tx
                .send(msg)
                .unwrap_or_else(|e| {
                    eprintln!("Error while sending message: {}", e);
                })
                .await;
        }
    });
    users.write().await.insert(id, tx);

    while let Some(result) = user_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("Error while handling message: {}", e);
                break;
            }
        };
        handle_message(id, msg, &users).await;
    }
    handle_disconnection(id, &users).await;
}

async fn handle_message(id: usize, msg: Message, users: &Users) {
    let request = if let Ok(s) = msg.to_str() {
        s
    } else {
        return;
    };

    let split_request: Vec<&str> = request.split_whitespace().collect();
    let response: String = if split_request.len() == 2 {
        let str1 = split_request[0];
        let str2 = split_request[1];
        format!(
            "{} {} {} {} {}",
            str1,
            str2,
            string_distance::hamming_normalized_similarity(str1, str2),
            strsim::jaro(str1, str2),
            strsim::normalized_levenshtein(str1, str2)
        )
    } else {
        return;
    };
    println!("User #{} response: {}", id, &response);
    for (_uid, tx) in users.read().await.iter() {
        if let Err(_disconnected) = tx.send(Message::text(response.clone())) {}
    }
}

async fn handle_disconnection(id: usize, users: &Users) {
    println!("User with id {} disconnected", id);
    users.write().await.remove(&id);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test() {
        let users = Users::default();
        let users = warp::any().map(move || users.clone());
        let filter = warp::ws().and(users).map(|ws: warp::ws::Ws, users| {
            ws.on_upgrade(move |socket| handle_connection(socket, users))
        });

        let mut client = warp::test::ws().handshake(filter).await.expect("handshake");

        client.send_text("tesst test").await;

        let msg = client.recv().await.expect("recv");
        assert_eq!(msg.to_str(), Ok("tesst test 0.6 0.9333333333333332 0.8"));
    }
}
